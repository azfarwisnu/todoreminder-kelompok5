# TodoReminder-Kelompok5

Kelompok  5 - 2IA02 

Perancangan Aplikasi Mobile
To Do Reminder Sederhana Berbasis Android

1. Andika Wahyu Permadi
2. Ikin Sugiharto
3. Muhammad Azfar Wisnu
4. Muhammad Fadhil Alhafizh
5. Nazhirul Wahidi
6. Raka Sayidina Riyadi

# Requirements

- Android Studio 2021.2.1.15
- Anrdoid Oreo version and API 26
- Java Version 8 Update 333

# Jurnal
K. I. R. Costa, “Rancang Bangun Aplikasi Mobile To Do List Sederhana Berbasis Android”, researchgate, vol. July, 7, 2021.

R. Azmi, Rahmawati, “Perancangan Aplikasi Todolist Berbasis Android Menggunakkan Flutter SDK dan Database SQLITE”, proceeding.unindra,vol. Januari, 6, 2021.

To do reminder merupakan aplikasi pengingat daftar tugas-tugas yang harus dikerjakan, diselesaikan, diurutkan berdasarkan skala prioritas dan berdasarkan waktu tertentu, misalnya jam, hari atau minggu.To do reminder bagian dari perencanaan agar tugas-tugas yang hendak dikerjakan tidak bertabrakan satu dengan lainnya. 


# Fitur Tambahan
1. Sort deadline tugas terdekat
2. Set Format time
3. Add alert delete
4. Add fitur konfirmasi

# Black box testing
![image.png](./image.png)

# Preview
![image-1.png](./image-1.png)
![image-2.png](./image-2.png)

# Source
https://www.gwnbs.com/2020/06/aplikasi-pengingat-tugas-to-do-reminder-android.html.
